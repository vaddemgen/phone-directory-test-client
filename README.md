# Phone Directory Test Client

Test client from the [Phone Directory](https://gitlab.com/vaddemgen/phone-directory).

Run as spring boot application. It'll deploy at the port `8188`.

Go to the browser [http://localhost:8188/](http://localhost:8188/) and see the result :)