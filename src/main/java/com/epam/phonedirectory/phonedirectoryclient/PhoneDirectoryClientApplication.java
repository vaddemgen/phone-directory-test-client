package com.epam.phonedirectory.phonedirectoryclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhoneDirectoryClientApplication {

  public static void main(String[] args) {
    SpringApplication.run(PhoneDirectoryClientApplication.class, args);
  }
}
