package com.epam.phonedirectory.phonedirectoryclient.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor
@Getter
@Builder
public final class AuthDetails {

  private final String sessionId;
  private final String csrfToken;
}
