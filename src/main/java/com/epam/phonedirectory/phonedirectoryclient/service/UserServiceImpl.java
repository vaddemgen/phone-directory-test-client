package com.epam.phonedirectory.phonedirectoryclient.service;

import com.epam.phonedirectory.phonedirectoryclient.model.AuthDetails;
import java.util.List;
import java.util.Objects;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public final class UserServiceImpl implements UserService {

  private static final String URL = "http://localhost:8187";
  private static final String URL_API = URL + "/api/v1";

  @Override
  public String getUsers() {
    AuthDetails authDetails = authorize();
    String users = getUsers(authDetails);
    logout(authDetails);
    return users;
  }

  private void logout(AuthDetails authDetails) {
    var httpHeaders = getHttpHeaders(authDetails.getCsrfToken(), authDetails.getSessionId());
    var entity = new HttpEntity<>(httpHeaders);
    RestTemplate restTemplate = new RestTemplate();
    ResponseEntity<String> responseEntity = restTemplate.exchange(URL + "/logout",
        HttpMethod.POST, entity, String.class);
    if (responseEntity.getStatusCode() != HttpStatus.FOUND) {
      throw new IllegalArgumentException("API is not available");
    }
  }

  private String getUsers(AuthDetails authDetails) {
    var httpHeaders = getHttpHeaders(authDetails.getCsrfToken(), authDetails.getSessionId());
    var entity = new HttpEntity<>(httpHeaders);
    RestTemplate restTemplate = new RestTemplate();
    ResponseEntity<String> responseEntity = restTemplate.exchange(URL_API + "/users",
        HttpMethod.GET, entity, String.class);
    if (responseEntity.getStatusCode() != HttpStatus.OK) {
      throw new IllegalArgumentException("API is not available");
    }
    return responseEntity.getBody();
  }

  private AuthDetails authorize() {
    String csrfToken = getCsrfToken();

    // Setting data
    var entity = new HttpEntity<>(getLoginParams(), getHttpHeaders(csrfToken, null));
    RestTemplate restTemplate = new RestTemplate();
    ResponseEntity<String> responseEntity = restTemplate.exchange(URL + "/login",
        HttpMethod.POST, entity, String.class);

    if (responseEntity.getStatusCode() != HttpStatus.FOUND) {
      throw new IllegalArgumentException("API is not available");
    }

    return AuthDetails.builder()
        .csrfToken(csrfToken)
        .sessionId(getSessionId(responseEntity.getHeaders()))
        .build();
  }

  private LinkedMultiValueMap<String, String> getLoginParams() {
    var params = new LinkedMultiValueMap<String, String>();
    params.add("username", "vaddemgen@gmail.com");
    params.add("password", "q1w2e3r4");
    return params;
  }

  private HttpHeaders getHttpHeaders(String csrfToken, String sessionId) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    headers.setAccept(List.of(MediaType.APPLICATION_JSON));
    headers.set("X-XSRF-TOKEN", csrfToken);
    headers.add("Cookie", new CookieBuilder()
        .addCsrfToken(csrfToken)
        .addSessionId(sessionId)
        .build());
    return headers;
  }

  private String getCsrfToken() {
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders httpHeaders = restTemplate.headForHeaders(URL + "/login", Object.class);
    return Objects.requireNonNull(httpHeaders.get("Set-Cookie"))
        .stream()
        .filter(str -> str.startsWith("XSRF-TOKEN"))
        .map(str -> str.substring("XSRF-TOKEN=".length(), str.indexOf(';')))
        .findFirst()
        .orElseThrow();
  }

  private String getSessionId(HttpHeaders httpHeaders) {
    return Objects.requireNonNull(httpHeaders.get("Set-Cookie"))
        .stream()
        .filter(str -> str.startsWith("JSESSIONID"))
        .map(str -> str.substring("JSESSIONID=".length(), str.indexOf(';')))
        .findFirst()
        .orElseThrow();
  }
}

