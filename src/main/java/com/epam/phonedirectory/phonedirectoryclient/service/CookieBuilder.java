package com.epam.phonedirectory.phonedirectoryclient.service;

import static java.util.Objects.nonNull;

class CookieBuilder {

  private StringBuilder value = new StringBuilder();

  CookieBuilder addCsrfToken(String token) {
    if (nonNull(token)) {
      value.append("XSRF-TOKEN=")
          .append(token)
          .append("; ");
    }
    return this;
  }

  CookieBuilder addSessionId(String sessionId) {
    if (nonNull(sessionId)) {
      value.append("JSESSIONID=")
          .append(sessionId)
          .append("; ");
    }
    return this;
  }

  String build() {
    return value.toString();
  }
}
