package com.epam.phonedirectory.phonedirectoryclient.controller;

import com.epam.phonedirectory.phonedirectoryclient.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/")
@Controller
@AllArgsConstructor
public class TestController {

  private final UserService userService;

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> getUsers() {
    return ResponseEntity.ok(userService.getUsers());
  }
}
